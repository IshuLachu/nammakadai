package com.project.service;



import java.util.List;

import com.project.model.Login;

public interface StoreService {

	List<Login> verifyList();

	void save(Login login);

}
