<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="icon" href="/images/icons/Maincart.png"/>
<title>Namma Kadai</title>
<link rel="stylesheet"
      href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
      src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<style>
.error{
color:red;}
.column {
    float: middle;
    height: 300px;
}

.left{
  width:40%;
  height:100%;
  float:left;
}
.right {
  width: 40%;
  height:100%;
  float:right;
}

.middle {
  width: 60%;
  align:center;
  float:middle;
}


.row:after {
    content: "";
    display: table;
    clear: both;
}
</style>
</head>
<body>
<div style="background-color: black">
            <b
                  style="font-family: Bradley Hand ITC; font-size: 30px; color: white">Namma
                  Kadai</b><img src="/images/icons/Maincart.png" width="80" height="80"></img>
            <!--       <marquee direction="right"></marquee> -->
      </div>
<div class="row">
  <div class="column left" style="position: absolute; left:10%; width:100; height:100;">
    <marquee direction="down" width="80%" height="100%">
                        <img src="/images/adds/dressadd.png" width="50%" height="50%" /><br><br>
                        <img src="/images/adds/mobile.jpg" width="50%" height="50%"/><br><br>
                        <img src="/images/adds/add4.jpg" width="50%" height="50%"/><br> 
                        <img src="/images/adds/dress1ck.jpg" width="50%" height="50%"/><br>
                        <img src="/images/adds/add5.jpg" width="50%" height="50%"/>
                  </marquee>
  </div>
<div align="right"> <div class="column right" style="position: absolute; right:0%; left:60%; width:100; height:100;">
   <marquee direction="down" width="80%" height="100%">
                        <img src="/images/adds/toyadd.png" width="50%" height="50%"/><br>
                        <img src="/images/adds/add2.png" width="50%" height="50%"/><br>
                        <img src="/images/adds/toys.jpg" width="50%" height="50%"/><br>
                        <img src="/images/adds/add3.png" width="50%" height="50%"/>
                        <img src="/images/adds/adidas.jpg" width="50%" height="50%"/><br>
                  </marquee>
  </div></div>
  <center>
  <div class="column middle" style="align:center;">
  <center>
    <h1>
                  <b style="font-family: Goudy Old Style;">Register</b>
            </h1>
      </center>
      <form:form action="save" method="post"
                              modelAttribute="login">
                              <table>
                              <tr>
                                          <td><b style="font-family: Goudy Old Style;"><h3>Username</h3></b></td>
                                          <td>&nbsp;&nbsp;
                                          <td><form:input path="userName" />
                                                <form:errors path="userName" class="error"></form:errors></td>
                                    </tr>
                                    &nbsp;&nbsp; &nbsp;&nbsp;
                                    <tr>
                                          <td><b style="font-family: Goudy Old Style;"><h3>Email</h3></b></td>
                                          <td>&nbsp;&nbsp;
                                          <td><form:input path="emailId" />
                                                <form:errors path="emailId" class="error"></form:errors></td>
                                    </tr>
                                    &nbsp;&nbsp; &nbsp;&nbsp;
                                    <tr>
                                          <td><b style="font-family: Goudy Old Style;"><h3>Contact
                                                            Number</h3></b></td>
                                          <td>&nbsp;&nbsp;
                                          <td><form:input path="phoneNo" />
                                                <form:errors path="phoneNo" class="error"></form:errors></td>
                                    </tr>
                                    &nbsp;&nbsp; &nbsp;&nbsp;
                                    <tr>
                                          <td><b style="font-family: Goudy Old Style;"><h3>Address</h3></b></td>
                                          <td>&nbsp;&nbsp;
                                          <td><form:input path="address" />
                                                <form:errors path="address" class="error"></form:errors></td>
                                    </tr>
                                    &nbsp;&nbsp; &nbsp;&nbsp;
                                    <tr>
                                          <td><b style="font-family: Goudy Old Style;"><h3>Password</h3></b></td>
                                          <td>&nbsp;&nbsp;
                                          <td><form:password path="password" />
                                                <form:errors path="password" class="error"></form:errors></td>
                                    </tr>
                                    &nbsp;&nbsp; &nbsp;&nbsp;
                                    <tr>
                                          <td colspan="4" align="center"><input type="submit"
                                                class="btn btn-success" value="Register"
                                                style="font-family: Goudy Old Style; font-weight: bold; font-size: 20px;"></td>
                                    </tr>
                              </table>
                        </form:form>

  </div></center>
  </div>
</body>
</html>
