<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="icon" href="/images/icons/Maincart.png"/>
<title>Namma Kadai</title>
<style>
.error{
color:red;}
</style>
</head>
<body>
<div style="background-color: black ">
<b style="font-family:Bradley Hand ITC;font-size:30px;color:white">Namma Kadai</b><img src="/images/icons/Maincart.png" width="80" height="80"></img>
<!--       <marquee direction="right"></marquee> -->
      </div>

 <center><h1><b style="font-family:Goudy Old Style;">Login</b></h1></center>
	
	
    <div align="center">
       
        <form:form action="loginverify" method="post" modelAttribute="login">
       
        <table>
           <form:hidden path="userId"/>
            <tr>
                <td><b style="font-family:Goudy Old Style;"><h3>Username</h3></b></td><td>&nbsp;&nbsp;</td>
                <td><form:input path="userName" /><form:errors path="userName" class="error"></form:errors></td>
            </tr>
            &nbsp;&nbsp; &nbsp;&nbsp;
            <tr>
                <td><b style="font-family:Goudy Old Style;"><h3>Password</h3></b></td><td>&nbsp;&nbsp;</td>
                <td><form:password path="password" /><form:errors path="password" class="error"></form:errors></td>
            </tr>
            &nbsp;&nbsp; &nbsp;&nbsp;
            <tr>
                <td colspan="3" align="center"><input type="submit" value="Login" class="btn btn-success" style="font-family:Goudy Old Style;font-weight:bold;font-size:20px;"></td>
            </tr>
             </table>
            <p align="center"><a color="Green" href="/NammaKadai/register">New User?Register Here!</a></p>         
       
        </form:form>
    </div>
     <div align="center">
    <img src="/images/icons/bags.jpg"  style="background-image:cover"/>
    </div>
    
   
</body>
</html>